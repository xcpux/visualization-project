import requests
import json
import re
import numpy as np
import pandas as pd

LABELS_PATH = '../../data/class_labels_indices.csv'
DATA_PATH = '../../data/unbalanced_train_segments.csv'
SAVE_PATH = '../../save/'

def get_words(classes=dict(), api_key='AIzaSyBTWuPHd91hUbJup2ZA9b5Sut-z4h2t2LY', order='relevance', max_comments=100, data_df=None, print_summary=False, top_word_count=20):
    if data_df is None:
        data_df = pd.read_csv(DATA_PATH, skiprows=2, sep=r',\s+', encoding='utf-8', engine='python')
    for label, desc in classes.items():
        df = data_df[data_df['positive_labels'].str.contains(label)]
        words = np.array([])
        total_comments = 0
        for index, row in df.iterrows():
            video_id = row['YTID']
            request = requests.get('https://www.googleapis.com/youtube/v3/commentThreads?key={api_key}&textFormat=plainText&part=snippet&maxResults={max_results}&videoId={id}&order={order}'.format(api_key=api_key, max_results=max_comments, id=video_id, order=order)).json()
            if 'pageInfo' in request:
                total_comments += request['pageInfo']['totalResults']
                for item in request['items']:
                    comment = np.array(re.findall(r"[\w']+|[.,!?;]", item['snippet']['topLevelComment']['snippet']['textOriginal']))
                    words = np.hstack((words, comment))

        words = np.unique(words, return_counts=True)
        if print_summary:
            print('Finished retrieving words for "{}" music'.format(desc))
            print('Total number of comments = {}'.format(total_comments))
            print('Total word count = {}'.format(len(words[0])))
            print('Top {} words:'.format(top_word_count))
            top_words = np.argsort(words[1])[-top_word_count:]
            for index in top_words:
                print('{}\t{}'.format(words[0][index], words[1][index]))
        # NOTE: 
        # The following will save the words and frequency count array to a file in the SAVE_PATH dir. 
        # This directory must exist as there is no prior checking or automatic creation for it (consider adding later).
        # More importantly numpy will automatically convert the type of this array into an array of strings for both the
        # words and frequencies. This means to load back data, the frequencies array must be type casted to an integer
        # type in order to avoid errors.
        np.save(SAVE_PATH+desc, words)


if __name__ == "__main__":
    get_words(classes={'/t/dd00031': 'Happy', '/t/dd00032' : 'Funny', '/t/dd00033' : 'Sad', '/t/dd00034' : 'Tender', '/t/dd00035' : 'Exciting', '/t/dd00036' : 'Angry', '/t/dd00037' : 'Scary'}, print_summary=True, top_word_count=30)
