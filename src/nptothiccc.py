import numpy as np
import pandas as pd
import nltk
from nltk.corpus import stopwords
nltk.download('stopwords')

SAVE_PATH = '../save/'

stop_words=[
    u'for', u'in', u'a', u'it', u'this', u'that', u'I\'m', 
    u'u', u'you' ,u'I', u'?', u'!', u'.', u':', u',', u';',
    u'like'
]

def get_top(classes=list()):
    full_data = pd.DataFrame({'Mood' : [], 'Word': [],'Count': []})
    for mood in classes:
        np_array = np.load(SAVE_PATH+mood+".npy").astype('U')
        sw_np = np.array(stopwords.words('english')+stop_words).astype('U')
        indices = []
        for i in range(len(np_array[0])):
            is_stop_word = False
            for j in range(len(sw_np)):
                if np_array[0][i].lower() == sw_np[j].lower():
                    is_stop_word = True
                    break
            if not is_stop_word:
                indices.append(i)
        indices = np.array(indices)
        x = np.argsort(np_array[1][indices].astype(np.int64))[-30:]
        num_records = len(x)
        print(mood+':')
        print(np_array[0][indices][x])
        data = {
            'Mood' : [mood]*num_records,
            'Word': np_array[0][indices][x],
            'Count': np_array[1][indices][x]
        }
        full_data = pd.concat([full_data, pd.DataFrame(data)])
    full_data.to_csv('test.csv', index=False)


if __name__ == "__main__":
    get_top(classes=['Happy',  'Sad', 'Tender', 'Scary'])
