var defaultMood = 'Happy';
var allMoods = ['Happy', 'Funny', 'Sad', 'Tender', 'Exciting', 'Angry', 'Scary']
var moods_data = [];
var moods_domain = [];

function changeMood(){
    var dropdownMenu = document.getElementById("dropdownMenu");
    d3.select("#chart_display").selectAll("*").remove();
    defaultMood = dropdownMenu.options[dropdownMenu.selectedIndex].value
    loadGraph();
}

function loadData(){
    d3.csv("../data/test.csv", function(error, data){
        data.forEach(function(d) {
            d.Mood = d.Mood;
            d.Word = d.Word;
            d.Count = parseInt(d.Count);
        });
        moods_data = [];
        moods_domain = [];
        for(var i = 0; i < allMoods.length; i++){
            mood_data = [];
            moods_data[allMoods[i]] = mood_data;
            mood_domain = [];
            moods_domain[allMoods[i]] = mood_domain;
        }
        for(var i = 0; i < data.length; i++){
            moods_data[data[i]['Mood']].push({'Word': data[i]['Word'], 'Count': data[i]['Count']});
            moods_domain[data[i]['Mood']].push(data[i]['Word']);
        }
        loadGraph();
    }
    );

}

function loadGraph(){
    var svg = d3.select("#chart_display"),
        margin = 200,
        width = svg.attr("width") - margin,
        height = svg.attr("height") - margin;

    svg.append("text")
        .attr("transform", "translate(100,0)")
        .attr("x", 200)
        .attr("y", 50)
        .attr("font-size", "24px")
        .text("Data Frequency");

    var xScale = d3.scaleBand().range([0, width]);
        yScale = d3.scaleLinear().range([height, 0]);

    var g = svg.append("g")
        .attr("transform", "translate(" + 100 + "," + 100 + ")");

    xScale.domain(moods_data[defaultMood].map(function(d){return d.Word;}));
    yScale.domain([0, d3.max(moods_data[defaultMood], function(d){return d.Count;})]);

    g.append("g")
        .attr("transform", "translate(0," + height + ")")
        .call(d3.axisBottom(xScale)) 
        .append("text")
        .attr("y", height - 250)
        .attr("x", width - 100)
        .attr("text-anchor", "end")
        .attr("stroke", "black")
        .text("Words");

    g.append("g")
        .call(d3.axisLeft(yScale))
        .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", "-5.1em")
        .attr("text-anchor", "end")
        .attr("stroke", "black")
        .text("Count");

    g.selectAll(".bar")
        .data(moods_data[defaultMood])
        .enter().append("rect")
        .attr("class", "bar") 
        .attr("x", function(d) { return xScale(d.Word); }) 
        .attr("y", function(d) { return yScale(d.Count); })
        .attr("width", xScale.bandwidth())
        .attr("height", function(d) { return height - yScale(d.Count); });

}

loadData();
loadGraph();