const DATA_DIR = '..\\data\\bar_chart.csv';
const CHART_TITLE = 'Bar Chart'
const margin = 100;
const width  = 1500 - (margin<<1);
const height = 700 - (margin<<1);
var num_bins = 14;

function get_bin_index(data_entry, bins) {
    for (var i = 0; i < bins.length; ++i) {
        if (data_entry >= bins[i][0] && data_entry < bins[i][1]){
            return i;
        }
    }
    return -1;
}

function bin_data(data, bins) {
    var binned_data = new Array(bins.length).fill(0);
    for (let data_entry of data) {
        binned_data[get_bin_index(data_entry, bins)] += data_entry;
    }
    return binned_data;
}

function uniform_bins(num_bins, start, end) {
    var range = end - start + 1;
    var bins = [];
    const bin_width = ~~(range/num_bins)
    const buckets_to_under_assign = num_bins - range % num_bins;
    for (var i = start, j = 0; j < num_bins; ++j) {
        if (j < buckets_to_under_assign) {
            bins.push([i, i += bin_width]);
        }
        else {
            bins.push([i, i += bin_width + 1]);
        }
    }
    return bins;
}

function clean_data(data) {
    return data.filter(entry => {
        var valid_entry = true;
        for (var attr in entry) {
            if (isNaN(entry[attr])){
                valid_entry = false;
                break;
            }
        }
        return valid_entry;
    });
}

function load_data(filepath) {
    d3.csv(filepath, 
        function(data) {
            return {
                'Thicc1' : +data['Year'],
                'Thicc 2' : +data['Index Total'],
                '2 Thicc 4 school' : +data['Violent Total'],
                'Thicc Murder' : +data['Murder'],
                'Thicc Rape' : +data['Rape'],
                'Down with the thiccness' : +data['Robbery'],
                'Prophethicc' : +data['Aggravated Assault']
            };
        }, 
        build_bar_chart(margin, width, height, num_bins, null)
    );
}

function get_data_column(data, attribute) {
    return [...data.map(x => x[attribute])];
}

var selected_attribute = null;
function build_bar_chart(margin, width, height, num_bins, svg) {
    if (svg == null || svg == undefined) {
        svg = d3.select('svg').attr('width', width + (margin * 2.0)).attr('height', height + (margin * 2.0));
    }
    return function(data) {
        var categories = [data.columns[2]].concat(data.columns.slice(4, 14));
        selected_attribute = (selected_attribute == null)?  categories[0] : selected_attribute;

        svg.append('text')
            .attr('x', (width / 2))
            .attr('y', (margin / 3))
            .attr('id', 'chart_title')
            .text(CHART_TITLE);

        const chart = svg.append('g')
            .attr('id', 'bar_chart')
            .attr('class', 'chart')
            .attr('transform', `translate(${margin}, ${margin})`);

        cleaned_data = clean_data(data);

        var chart_data = new Array(num_bins);
        var histograms = [];
        for (var i = 0; i < categories.length; ++i) {
            var partial_data = get_data_column(cleaned_data, categories[i]);
            var bins = uniform_bins(num_bins, Math.min(...partial_data), Math.max(...partial_data));
            var binned_data = bin_data(partial_data, bins);
            histograms.push({
                attribute : categories[i],
                bins: bins,
                data : binned_data
            });
        }

        for (var i = 0; i < num_bins; ++i) {
            chart_data[i] = {index : i};
            for (var j = 0; j < histograms.length; ++j) {
                chart_data[i][histograms[j].attribute] = {
                    x : histograms[j].bins[i],
                    y : histograms[j].data[i],
                }
            }
        }

        var current_hist = histograms.filter(x => x.attribute == selected_attribute)[0];
        var bins = current_hist.bins;
        var binned_data = current_hist.data;
        
        var x_scale = d3.scaleBand()
            .range([0, width])
            .domain(bins.map(bin => '[' + bin.toString() + ')'))
            .padding(0.1);

        var y_scale = d3.scaleLinear()
            .range([height, 0])
            .domain([0, Math.max(...binned_data)]);

        var x_axis = d3.axisBottom(x_scale);
        var y_axis = d3.axisLeft(y_scale);

        var menu = d3.select('#chart_menu')
            .append('select')
            .attr('id', 'chart_menu_select')
            .on('change', handle_bar_chart_attribute_change(histograms, height, x_axis, y_axis, x_scale, y_scale));
            
        menu.selectAll('category')
            .data(categories)
            .enter()
            .append('option')
            .attr('id', x => x)
            .attr('value', x => x)
            .text(x => x);

        document.getElementById(selected_attribute).selected = true;

        var slider = d3.select('#slider_container')
            .append('input')
            .attr('id', 'bucket_slider')
            .attr('type', 'range')
            .attr('min', 3)
            .attr('max', 14)
            .attr('value', num_bins)
            .on('change', handle_slider_update(data, svg));
        
        chart.append('g')
            .attr('class', 'y_axis')
            .call(y_axis);

        chart.append('text')
            .attr('transform', 'rotate(-90)')
            .attr('y', - margin / 1.25)
            .attr('x',  - height / 2)
            .attr('class', 'axis_label')
            .text('Crime Index');
            
        chart.append('g')
            .attr('class', 'x_axis')
            .attr('transform', `translate(0, ${height})`)
            .call(x_axis);

        chart.append('text')
            .attr('transform', `translate(${(width - margin) / 2}, ${height + margin / 2})`)
            .attr('class', 'axis_label')
            .attr('id', 'x_axis_label')
            .text(selected_attribute);
        
        var color_scale = d3.scaleOrdinal(d3.schemeCategory20c);

        chart.selectAll('.bar')
            .data(chart_data)
            .enter()
            .append('rect')
            .attr('class', 'bar')
            .attr('id', entry => 'bar_id-'+entry.index)
            .attr('x', entry => x_scale('[' + entry[selected_attribute].x + ')'))
            .attr('y', entry => y_scale(entry[selected_attribute].y))
            .attr('height', entry => height - y_scale(entry[selected_attribute].y))
            .attr('width', x_scale.bandwidth())
            .style('fill', (d,i) => color_scale(i));

        chart.selectAll('.text')
            .data(chart_data)
            .enter()
            .append('text')
            .attr('class', 'bar_label')
            .attr('id', entry => 'bar_label-'+entry.index)
            .text(entry => entry[selected_attribute].y)
            .attr('x', entry => x_scale('[' + entry[selected_attribute].x + ')') + x_scale.bandwidth() / 2)
            .attr('y', entry => y_scale(entry[selected_attribute].y) - 5)
            .style('opacity', 0.0);
        
        const label_transition_duration = 200;
        const x_delta = 10, y_delta = 20;

        chart.selectAll('.bar')
            .on('mouseover', function(d, i) {
                var selected_attribute = document.getElementById('chart_menu_select').value;
                d3.select('#bar_label-'+i)
                    .transition()
                    .duration(label_transition_duration)
                    .style('opacity', 1.0)
                    .attr('x', entry => 
                        x_scale('[' + entry[selected_attribute].x + ')') 
                        + ((x_scale.bandwidth() + x_delta) / 2)
                        - (x_delta / 2)
                    )
                    .attr('y', entry => y_scale(entry[selected_attribute].y) - 5 - y_delta);
                d3.select('#bar_id-'+i)
                    .transition(label_transition_duration)
                    .attr('x', entry => x_scale('[' + entry[selected_attribute].x + ')') - (x_delta / 2))
                    .attr('y', entry => y_scale(entry[selected_attribute].y) - y_delta)
                    .attr('height', entry => height - y_scale(entry[selected_attribute].y) + y_delta)
                    .attr('width', x_scale.bandwidth() + x_delta);
            })
            .on('mouseout', function(d, i) {
                var selected_attribute = document.getElementById('chart_menu_select').value;
                d3.select('#bar_label-'+i)
                    .transition()
                    .duration(label_transition_duration)
                    .attr('x', entry => x_scale('[' + entry[selected_attribute].x + ')') + x_scale.bandwidth() / 2)
                    .attr('y', entry => y_scale(entry[selected_attribute].y) - 5)
                    .style('opacity', 0.0);
                d3.select('#bar_id-'+i)
                    .transition(label_transition_duration)
                    .attr('x', entry => x_scale('[' + entry[selected_attribute].x + ')'))
                    .attr('y', entry => y_scale(entry[selected_attribute].y))
                    .attr('height', entry => height - y_scale(entry[selected_attribute].y))
                    .attr('width', x_scale.bandwidth());
            });

        svg.on('click', toggle_chart(data, svg));
    }
}

function handle_bar_chart_attribute_change(histograms, height, x_axis, y_axis, x_scale, y_scale) {
    return function () {
        const transition_duration = 1000;
        const transition_ease = d3.easeSin;
        selected_attribute = document.getElementById('chart_menu_select').value;
        var current_hist = histograms.filter(x => x.attribute == selected_attribute)[0];
        var bins = current_hist.bins;
        var binned_data = current_hist.data;

        x_scale.domain(bins.map(bin => '[' + bin.toString() + ')'));
        y_scale.domain([0, Math.max(...binned_data)]);

        x_axis.scale(x_scale);
        y_axis.scale(y_scale);

        d3.selectAll('.bar')
            .transition()
            .ease(transition_ease)
            .duration(transition_duration)
            .attr('x', entry => x_scale('[' + entry[selected_attribute].x + ')'))
            .attr('y', entry => y_scale(entry[selected_attribute].y))
            .attr('height', entry => height - y_scale(entry[selected_attribute].y))
            .attr('width', x_scale.bandwidth());
        
        d3.selectAll('.bar_label')
            .transition()
            .ease(transition_ease)
            .duration(transition_duration)
            .text(entry => entry[selected_attribute].y)
            .attr('x', entry => x_scale('[' + entry[selected_attribute].x + ')') + x_scale.bandwidth() / 2)
            .attr('y', entry => y_scale(entry[selected_attribute].y) - 5);

        d3.selectAll('g.x_axis')
            .transition()
            .duration(transition_duration>>1)
            .call(x_axis);
        
        d3.selectAll('g.y_axis')
            .transition()
            .call(y_axis);

        d3.select('#x_axis_label')
            .transition()
            .duration(transition_duration>>1)
            .text(selected_attribute);
    };
}

function build_pie_chart(margin, num_bins, svg) {
    if (svg == null || svg == undefined) {
        svg = d3.select('svg')
            .attr('width', width + (margin * 2.0))
            .attr('height', height + (margin * 2.0));
    }
    return function(data) {
        const pie_char_inner_radius = 0;
        var categories = [data.columns[2]].concat(data.columns.slice(4, 14));

        svg.append('text')
            .attr('x', (width / 2))
            .attr('y', (margin / 2))
            .attr('id', 'chart_title')
            .text(CHART_TITLE);

        const chart = svg.append('g')
            .attr('id', 'pie_chart')
            .attr('class', 'chart')
            .attr('transform', `translate(${width / 2.0 + margin}, ${height / 2.0 + margin})`);

            cleaned_data = clean_data(data);

            var chart_data = new Array(num_bins);
            selected_attribute = (selected_attribute == null)?  categories[0] : selected_attribute;
            var histograms = [];
            for (var i = 0; i < categories.length; ++i) {
                var partial_data = get_data_column(cleaned_data, categories[i]);
                var bins = uniform_bins(num_bins, Math.min(...partial_data), Math.max(...partial_data));
                var binned_data = bin_data(partial_data, bins);
                histograms.push({
                    attribute : categories[i],
                    bins: bins,
                    data : binned_data
                });
            }
    
            for (var i = 0; i < num_bins; ++i) {
                chart_data[i] = {index : i};
                for (var j = 0; j < histograms.length; ++j) {
                    chart_data[i][histograms[j].attribute] = {
                        x : histograms[j].bins[i],
                        y : histograms[j].data[i],
                    }
                }
            }
    
            var current_hist = histograms.filter(x => x.attribute == selected_attribute)[0];
            var bins = current_hist.bins;
            var binned_data = current_hist.data;
            
            var color_scale = d3.scaleOrdinal(d3.schemeCategory20c);
            
            var sum_total = binned_data.reduce((acc, val) => acc + val, 0) * 1.0;
            var radius = Math.min(width, height) / 2.0;

            var arc = d3.arc()
                .outerRadius(radius)
                .innerRadius(pie_char_inner_radius);

            var pie = d3.pie()
                .value(x => (x / sum_total) * radius);

            var arcs = chart.selectAll('.arc')
                .data(pie(binned_data))
                .enter()
                .append('g')
                .attr('class', 'arc');

            arcs.append('path')
                .attr('d', arc)
                .style('fill', (d,i) => color_scale(i));

            svg.append('text')
                .attr('transform', `translate(${width / 8.0 + margin}, ${margin})`)
                .attr('class', 'axis_label')
                .text(selected_attribute);
            
            var legend = svg.selectAll('.legend')
                .data(pie(binned_data))
                .enter()
                .append('g')
                .attr('transform', (d, i) => `translate(
                    ${width / 8.0}, 
                    ${margin + i * 20 + 20}
                )`)
                .attr('class', 'legend');
            
            legend.append('rect')
                .attr('width', 12)
                .attr('height', 12)
                .attr('fill', (d,i) => color_scale(i));

            legend.append('text')
                .text((d, i) => '['+bins[i]+'): '+d.data + ' (' + (d.data/sum_total * 100.).toFixed(2) + ')%')
                .style('font-size', 12)
                .attr('x', 16)
                .attr('y', 9);
    };
}

const chart_types = {
    bar: 0,
    pie: 1
};

var current_chart = chart_types.bar;

function toggle_chart(data, svg) {
    return function() {
        clean_up();
        if (current_chart == chart_types.pie) {
            current_chart = chart_types.bar;
            return build_bar_chart(margin, width, height, num_bins, svg)(data);
        }
        else {
            current_chart = chart_types.pie;
            return build_pie_chart(margin, num_bins, svg)(data);
        }
    }
}

function handle_slider_update(data, svg) {
    return function() {
        num_bins = document.getElementById('bucket_slider').value;
        clean_up();
        return build_bar_chart(margin, width, height, num_bins, svg)(data);
    };
}

function clean_up() {
    document.getElementById('chart_display').innerHTML = '';
    document.getElementById('chart_menu').innerHTML = '';
    document.getElementById('slider_container').innerHTML = '';
    d3.selectAll('*').exit().remove();
}

load_data(DATA_DIR);
